package main

import (
	"database/sql"
	"log"
	"net/http"
	"time"

	mgo "gopkg.in/mgo.v2"
)

func trace(name string, r *http.Request) {
	log.Printf(
		"%s\t%s\t%s\t%s",
		r.Method,
		r.RequestURI,
		name,
		time.Since(time.Now()),
	)
}

// utility to check connection to database
func checkPostgresql(db *sql.DB) string {
	var current string

	queryStmt, err := db.Prepare(CURRENT_TIME)
	if err != nil {
		log.Fatal(err)
	}
	err = queryStmt.QueryRow().Scan(&current)
	return current
}

func checkMongodb(mgo *mgo.Database) string {
	var current string

	return current
}

const CURRENT_TIME = "SELECT current_timestamp as result"
