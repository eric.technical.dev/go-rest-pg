package main

import (
	"database/sql"
	"fmt"
	"log"

	"github.com/gorilla/mux"
	mgo "gopkg.in/mgo.v2"
)

type Application struct {
	Router *mux.Router
	// postgresql connection
	DB *sql.DB
	// mongo connection
	MGO *mgo.Database
}

func (a *Application) Initialize(host, port, user, password, dbname, mongoServer, mongoDatabase string) {
	a.InitializePostgresql(host, port, user, password, dbname)
	a.InitializeMongodb(mongoServer, mongoDatabase)

	a.Router = mux.NewRouter()
	a.initializeRoutes()
}

func (a *Application) InitializePostgresql(host, port, user, password, dbname string) {
	// db, err = sql.Open("postgres", "host=postgres port=5432 user=docker password=docker sslmode=disable ")
	connectionString :=
		fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)

	var err error
	a.DB, err = sql.Open("postgres", connectionString)
	if err != nil {
		log.Fatal(err)
	}
}

func (a *Application) InitializeMongodb(mongoServer, mongoDatabase string) {

	session, err := mgo.Dial(mongoServer)
	if err != nil {
		log.Fatal(err)
	}
	a.MGO = session.DB(mongoDatabase)
}

func (a *Application) initializeRoutes() {
	a.Router.HandleFunc("/products", a.findAllProducts).Methods("GET")
	a.Router.HandleFunc("/products", a.createProduct).Methods("POST")
	a.Router.HandleFunc("/products/{id:[0-9]+}", a.findProduct).Methods("GET")
	a.Router.HandleFunc("/products/{id:[0-9]+}", a.updateProduct).Methods("PUT")
	a.Router.HandleFunc("/products/{id:[0-9]+}", a.deleteProduct).Methods("DELETE")

	a.Router.HandleFunc("/movies", a.findAllMovies).Methods("GET")
	a.Router.HandleFunc("/movies", a.createMovie).Methods("POST")
	a.Router.HandleFunc("/movies", a.updateMovie).Methods("PUT")
	a.Router.HandleFunc("/movies", a.deleteMovie).Methods("DELETE")
	a.Router.HandleFunc("/movies/{id}", a.findMovie).Methods("GET")

	a.Router.HandleFunc("/ping", ping).Methods("GET")
	a.Router.HandleFunc("/health", a.healthCheck).Methods("GET")

}
